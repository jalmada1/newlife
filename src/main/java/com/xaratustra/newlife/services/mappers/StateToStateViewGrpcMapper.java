package com.xaratustra.newlife.services.mappers;

import java.util.ArrayList;
import java.util.HashMap;

import com.xaratustra.newlife.services.grpc.Livi;
import com.xaratustra.newlife.services.grpc.StateView;
import com.xaratustra.newlife.services.models.Cell;
import com.xaratustra.newlife.services.models.State;

import org.springframework.stereotype.Component;

@Component
public class StateToStateViewGrpcMapper {

    public StateView StateToStateView(State<com.xaratustra.newlife.services.models.Livi> source){

        HashMap<String, Cell<com.xaratustra.newlife.services.models.Livi>> results = source.getResult();

        ArrayList<Livi> livisToAdd = new ArrayList<>();

        for(int x = 0; x < source.getSizeX(); x++){
            for(int y = 0; y < source.getSizeY(); y++){
                String resultKey = Cell.toCoordinatesString(x, y);

                if(results.containsKey(resultKey) && results.get(resultKey) != null && results.get(resultKey).getContents().isPresent()){
                    com.xaratustra.newlife.services.models.Livi livi = results.get(resultKey).getContents().get();
                    Livi liviToAdd = Livi.newBuilder()
                        .setAge(livi.getAge())
                        .setColor(livi.getColor())
                        .setFirstName(livi.getFirstName())
                        .setLastName(livi.getLastName())
                        .setX(x)
                        .setY(y)
                        .build();
                    livisToAdd.add(liviToAdd);
                }
            }
        }

        return StateView.newBuilder()
            .setSizeX(source.getSizeX())
            .setSizeY(source.getSizeY())
            .addAllLivis(livisToAdd)
        .build();
    }    
}
