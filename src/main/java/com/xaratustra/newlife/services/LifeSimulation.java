package com.xaratustra.newlife.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import com.xaratustra.newlife.services.models.Cell;
import com.xaratustra.newlife.services.models.Livi;
import com.xaratustra.newlife.services.models.State;

import org.springframework.stereotype.Component;

@Component
public class LifeSimulation {

    private HashMap<String, Cell<Livi>> occupiedCells;

    private static final int DEFAULT_SIZE_X = 32;
    private static final int DEFAULT_SIZE_Y = 32;
    private static final int DEFAULT_POPULATION_SIZE = 200;
    private int matrixSize_x;
    private int matrixSize_y;
    private int populationSize;

    private int historyLength = 3;
    private LinkedList<String> stateHistory;


    public LifeSimulation() {
        this(DEFAULT_SIZE_X, DEFAULT_SIZE_Y, DEFAULT_POPULATION_SIZE);
    }

    public LifeSimulation(int matrixSize_x, int matrixSize_y, int populationSize) {
        this.matrixSize_x = matrixSize_x;
        this.matrixSize_y = matrixSize_y;
        this.populationSize = populationSize;
        this.stateHistory = new LinkedList<>();
    }

    public State<Livi> run() {
        this.occupiedCells = this.occupiedCells == null || hasLastHistoryEventHappenedBefore()
            ? getRandomInitialState(matrixSize_x, matrixSize_y, populationSize) 
            : run(this.occupiedCells);

        State<Livi> result = new State<Livi>(this.occupiedCells, matrixSize_x, matrixSize_y);

        return result;
    }

    private void addToHistory(String event){
        if(stateHistory.size() >= historyLength){
            stateHistory.poll();
        }

        stateHistory.add(event);
    }

    private boolean hasLastHistoryEventHappenedBefore(){

        if(stateHistory.isEmpty() || stateHistory.size() < historyLength){
            return false;
        }

        String lastStateHistory = stateHistory.peekLast();

        for(int i = 0; i < stateHistory.size() - 1; i++){
            if(stateHistory.get(i).equals(lastStateHistory)){
                return true;
            }
        }

        return false;
    }

    private HashMap<String, Cell<Livi>> run(HashMap<String, Cell<Livi>> prevState){
        HashMap<String, Cell<Livi>> prevStateWithNeighborsCountUpdated = countNeighbours(prevState);
        HashMap<String, Cell<Livi>> newState = runRules(prevStateWithNeighborsCountUpdated);

        return newState;    
    }

    private HashMap<String, Cell<Livi>> countNeighbours(HashMap<String, Cell<Livi>> prevState){
        HashMap<String, Cell<Livi>> newState = new HashMap<String, Cell<Livi>>();

        prevState.keySet().stream().forEach(key -> {
            Cell<Livi> cell = prevState.get(key);

            for(int x = cell.getX() -1; x <= cell.getX() + 1; x++){
                if(x >= matrixSize_x || x < 0){
                    continue;
                }
                for(int y = cell.getY() -1; y <= cell.getY() + 1; y++){
                    if(y >= matrixSize_y || y < 0 || (x == cell.getX() && y == cell.getY())){
                        continue;
                    }
                
                    String newStateCoordinates = Cell.toCoordinatesString(x, y);
                    Cell<Livi> newStateCell = new Cell<Livi> (
                        x,
                        y,
                        newState.containsKey(newStateCoordinates) 
                            ? newState.get(newStateCoordinates).getIsOccupied() 
                            : (prevState.containsKey(newStateCoordinates) 
                                ? prevState.get(newStateCoordinates).getIsOccupied() 
                                : false),
                        !newState.containsKey(newStateCoordinates) ? 1 : newState.get(newStateCoordinates).getNeighborsCount() + 1,
                        newState.containsKey(newStateCoordinates)  && newState.get(newStateCoordinates).getContents().isPresent()
                            ? newState.get(newStateCoordinates).getContents().get() 
                            : (prevState.containsKey(newStateCoordinates) && prevState.get(newStateCoordinates).getContents().isPresent()
                                ? prevState.get(newStateCoordinates).getContents().get() 
                                : null)
                    );

                    newState.put(newStateCoordinates, newStateCell);
                }
            }
        });

         return newState;
    }
    
    private HashMap<String, Cell<Livi>> runRules(HashMap<String, Cell<Livi>> prevStateWithNeighborsCountUpdated) {
        HashMap<String, Cell<Livi>> newState = new HashMap<String, Cell<Livi>>();


        String stateHistoryEvent = "";

        for(String key : prevStateWithNeighborsCountUpdated.keySet()) {

            Cell<Livi> cellPrevState = prevStateWithNeighborsCountUpdated.get(key);
            boolean isOccupied = cellPrevState != null && cellPrevState.getIsOccupied();

            boolean newCellState = runCellRules(isOccupied, prevStateWithNeighborsCountUpdated.get(key).getNeighborsCount());

            if(newCellState){

                int x = cellPrevState.getX();
                int y = cellPrevState.getY();

                if(prevStateWithNeighborsCountUpdated.get(key).getNeighborsCount() == 3 && !isOccupied){
                    String keyToInheritFrom = getKeyToInheritFrom(x, y, prevStateWithNeighborsCountUpdated);

                    if(keyToInheritFrom != ""){
                        String lastName = prevStateWithNeighborsCountUpdated.get(keyToInheritFrom).getContents().get().getLastName();
                        String color = prevStateWithNeighborsCountUpdated.get(keyToInheritFrom).getContents().get().getColor();

                        newState.put(key, new Cell<Livi>(cellPrevState.getX(), cellPrevState.getY(), true, 0,  new Livi(lastName, color)));
                    }
                } else {
                    newState.put(key, new Cell<Livi>(cellPrevState.getX(), cellPrevState.getY(), true, 0,  cellPrevState.getContents().get().update()));
                }

                //TODO: Get an alternative to text key
                stateHistoryEvent += (x + "" + y);
            }
        }

        addToHistory(stateHistoryEvent);

        return newState;
    }

    private String getKeyToInheritFrom(int x, int y, HashMap<String, Cell<Livi>> prevStateWithNeighborsCountUpdated) {

        List<Cell<Livi>> occupiedCells = new ArrayList<>();

        int xFrom = x-1;
        int xTo = x+1;
        int yFrom = y-1;
        int yTo = y+1;

        xFrom = xFrom < 0 ? 0 : xFrom;
        xTo = xTo >= this.matrixSize_x ? (this.matrixSize_x - 1) : xTo;
        yFrom = yFrom < 0 ? 0 : yFrom;
        yTo = yTo >= this.matrixSize_y ? (this.matrixSize_y - 1) : yTo;


        for(int prevX = xFrom; prevX <= xTo; prevX++){
            for(int prevY = yFrom; prevY <= yTo; prevY++){
                if(prevX != x || prevY != y){
                    Cell<Livi> prevCell = prevStateWithNeighborsCountUpdated.get(prevX + "-" + prevY);
                    if(prevCell != null && !prevCell.getContents().isEmpty() && prevCell.getIsOccupied()){
                        occupiedCells.add(prevCell);
                    }
                }
            }
        }

        if(occupiedCells.size() == 0){
            return "";
        }

        int randomIndex = getRandomNumber(0, occupiedCells.size() - 1);
        Cell<Livi> cellToInheritFrom = occupiedCells.get(randomIndex);

        return cellToInheritFrom.toCoordinatesString();
    }

    private int getRandomNumber(int from, int to){
        int max = to;
        int min = from;
        int range = max - min + 1;

        int random = (int)(Math.random() * range) + min;
        return random;
    }


    private boolean runCellRules(boolean isOccupied, int neighboursCount){
        boolean newState = false;

        if (isOccupied && neighboursCount < 2){ // Any live cell with fewer than two live neighbours dies, as if by underpopulation.
            newState = false;
        } else if (isOccupied && neighboursCount < 4){ // Any live cell with two or three live neighbours lives on to the next generation.
            newState = true;
        } else if (isOccupied && neighboursCount >= 4){ // Any live cell with more than three live neighbours dies, as if by overpopulation.
            newState = false;
        } else if (!isOccupied && neighboursCount == 3){ // Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
            newState = true;
        }

        return newState;
    }

    private HashMap<String, Cell<Livi>> getRandomInitialState(int matrixSize_x, int matrixSize_y, int populationSize){
        HashMap<String, Cell<Livi>> newOccupiedCells = new HashMap<String, Cell<Livi>>();

        int currentPopulationSize = 0;

        while(currentPopulationSize < populationSize){

            int randomX = getRandomPosition(matrixSize_x, 0);
            int randomY = getRandomPosition(matrixSize_y, 0);

            Cell<Livi> newCell = new Cell<Livi>(randomX, randomY, true, 0, new Livi());
            String newCellCoordinates = newCell.toCoordinatesString();

            if(newOccupiedCells.containsKey(newCellCoordinates)){
                continue;
            }

            newOccupiedCells.put(newCellCoordinates, newCell);
            currentPopulationSize++;
        }
        this.stateHistory = new LinkedList<>();
        return newOccupiedCells;
    }

    private int getRandomPosition(int max, int min){

        max = max - 1;
        int range = max - min + 1;

        return (int)(Math.random() * range) + min;
    }
}
