package com.xaratustra.newlife.services.models;

import java.util.Random;

public class Livi {
    private int age;
    private String firstName;
    private String lastName;
    private String color;

    public String getColor() {
        return this.color;
    }

    public void setColor(String color) {
        this.color = color;
    }
    
    public void setAge(int age) {
        this.age = age;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return this.age;
    }

    public Livi update() {
        this.age += 1;
        return this;
    }
    
    public Livi() {
        this(0);
    }

    private Livi(int age) {
        this.age = age;
        this.firstName = generateRandomString();
        this.lastName = generateRandomString();
        this.color = generateRandomColor();
    }

    public Livi(String lastName, String color) {
        this(0);
        this.lastName = lastName;
        this.color = color;
    }

    private String generateRandomString(){
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();

        String generatedString = random.ints(leftLimit, rightLimit + 1)
        .limit(targetStringLength)
        .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
        .toString();

        return generatedString;
    }

    private String generateRandomColor(){
        Random random = new Random();
        int nextInt = random.nextInt(0xffffff + 1);


        String color = Integer.toHexString(nextInt);
        
        if(nextInt < 10000){
            color = '0' + color;
        }

        if(nextInt < 100000){
            color = '0' + color;
        }

        return color;
    }
}
