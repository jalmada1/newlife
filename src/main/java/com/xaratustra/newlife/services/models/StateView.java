package com.xaratustra.newlife.services.models;


public class StateView<T> {
    private final int size_x;
    private final int size_y;
    private final T[][] grid;

    public StateView(int size_x, int size_y, T[][] grid) {
        this.size_x = size_x;
        this.size_y = size_y;
        this.grid = grid;
    }

    public int getSizeX() {
        return this.size_x;
    }

    public int getSizeY() {
        return this.size_y;
    }

    public T[][] getGrid() {
        return this.grid;
    }
}
