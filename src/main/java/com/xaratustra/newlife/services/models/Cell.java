package com.xaratustra.newlife.services.models;

import java.util.Optional;

public class Cell<T> {
    private int x;
    private int y;
    private boolean isOccupied = false;
    private int neighborsCount = 0;
    private Optional<T> contents;

    public Cell(
    int x, 
    int y, 
    boolean isOccupied, 
    int neighborsCount,
    T contents) {
        this.x = x;
        this.y = y;
        this.isOccupied = isOccupied;
        this.neighborsCount = neighborsCount;
        this.contents = contents == null ? 
            Optional.empty() : Optional.of(contents);
    }

    public int getX(){
        return x;
    }

    public int getY(){
        return y;
    }

    public boolean getIsOccupied(){
        return isOccupied;
    }

    public int getNeighborsCount(){
        return neighborsCount;
    }

    public Optional<T> getContents(){
        return contents;
    }

    public static String toCoordinatesString(int x, int y){
        return String.valueOf(x) + "-" + String.valueOf(y);
    }

    public String toCoordinatesString(){
        return toCoordinatesString(x, y);
    }
}
