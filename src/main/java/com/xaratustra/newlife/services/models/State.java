package com.xaratustra.newlife.services.models;

import java.util.HashMap;

import org.apache.commons.text.StringEscapeUtils;

public class State<T> {

    private final HashMap<String, Cell<T>> result;
    private final int size_x;
    private final int size_y;
   
    public State(HashMap<String, Cell<T>>  result, int size_x, int size_y) {
        this.result = result;
        this.size_x = size_x;
        this.size_y = size_y;
    }

    public HashMap<String, Cell<T>> getResult() {
        return result;
    }

    public int getSizeX() {
        return this.size_x;
    }

    public int getSizeY() {
        return this.size_y;
    }

    public String toString(){
        return toString(false);
    }

    public String toHtmlTable(){
        StringBuilder table = new StringBuilder();

        if(result == null){
            return "";
        }

        boolean[][] matrix = new boolean[size_x][size_y];
        result.values().stream().forEach(cell -> {
            matrix[cell.getX()][cell.getY()] = true;
        });


        table.append("<table>");

        for(int x = 0; x < size_x; x++){
            table.append("<tr>");
            for(int y = 0; y < size_y; y++){
                table.append("<td>"+ (matrix[x][y] ? "O" : " ") + "</td>");
            }
            table.append("</tr>");
        }

        table.append("</table>");

        return table.toString();
    }

    public String toHTMLString(){
        return StringEscapeUtils.escapeHtml4(toString(false));
    }

    public String toString(boolean verbose) {
        
        if(result == null){
            return "";
        }

        boolean[][] matrix = new boolean[size_x][size_y];
        result.values().stream().forEach(cell -> {
            matrix[cell.getX()][cell.getY()] = true;
        });

        StringBuilder strBuilder = new StringBuilder();

        for(int x = 0; x < size_x; x++){
            for(int y = 0; y < size_y; y++){
                strBuilder.append("["+ (matrix[x][y] ? "O" : " ") + "]");
            }
            strBuilder.append("\r\n");
        }

        return strBuilder.toString();
    }
    
}
