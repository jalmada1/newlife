package com.xaratustra.newlife.services.grpc;

import org.springframework.beans.factory.annotation.Autowired;

import com.xaratustra.newlife.services.LifeSimulation;
import com.xaratustra.newlife.services.mappers.StateToStateViewGrpcMapper;
import com.xaratustra.newlife.services.models.Livi;

import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;

@GrpcService
public class LifeSimulationServiceImpl extends LifeSimulationServiceGrpc.LifeSimulationServiceImplBase 
{

    @Autowired
    StateToStateViewGrpcMapper stateToStateViewGrpcMapper;

    @Autowired
    public LifeSimulationServiceImpl() 
    {
        super();
    }
    
    @Override
    public void simulate(LifeSimulationRequest request, StreamObserver<LifeSimulationReply> responseObserver) {

            int times = request.getTimes();
            LifeSimulation lifeSimulation = new LifeSimulation();

            boolean forever = times == -1;

            while(times-- > 0 || forever){
                StateView stateView = stateToStateViewGrpcMapper.StateToStateView(lifeSimulation.run());
                LifeSimulationReply reply = LifeSimulationReply.newBuilder().setStateView(stateView).build();
                responseObserver.onNext(reply);
            }

            responseObserver.onCompleted();
    }
}
